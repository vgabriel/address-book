<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model {

    protected $fillable = [
        'name',
        'company',
        'address',
        'phone_number',
        'email',
        'notes'
    ];

    public function contacts()
    {
        return $this->hasMany('App\Record');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
