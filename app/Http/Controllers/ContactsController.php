<?php namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests;
use App\Http\Requests\PrepareContactRequest;
use App\Record;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ContactsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view('contacts.create');
    }

    public function __construct()
    {
        // Allow only logged in users
        $this->middleware('auth');
    }

    /**
     * View contacts for a particular user
     *
     * @param Guard $auth
     * @return \Illuminate\View\View
     * @internal param Album $album
     */
    public function viewContacts(Guard $auth)
    {
        $records = $auth->user()->records()->get();

        return view('contacts.view', compact('records'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        Record::where('id', '=', $id)->delete();

        Session::flash('message', "Contact Deleted");

        return Redirect::back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * PrepareContactRequest will validate inputs
     *
     * @param PrepareContactRequest $request
     * @param Guard $auth
     * @return Response
     */
	public function store(PrepareContactRequest $request, Guard $auth)
	{
        $contact = ['name'          => $request->input('name'),
                    'company'       => $request->input('company'),
                    'address'       => $request->input('address'),
                    'phone_number'  => $request->input('phone_number'),
                    'email'         => $request->input('email'),
                    'notes'         => $request->input('notes'),
        ];

        $auth->user()->records()->create($contact);

        return redirect('contacts/view');
	}

}
