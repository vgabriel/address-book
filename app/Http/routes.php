<?php

/*
 * The home page
*/
Route::get('/', 'PagesController@home');

/*
 * Authentication
*/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
 * Contacts
 */
Route::get('contacts/create', 'ContactsController@index');
Route::post('contacts/create', 'ContactsController@store');

// Delete contacts
Route::delete('contacts/destroy/{id}', array('uses' => 'ContactsController@destroy', 'as' => 'delete_record'));

Route::get('contacts/view', 'ContactsController@viewContacts');