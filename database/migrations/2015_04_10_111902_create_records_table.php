<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('records', function(Blueprint $table)
        {
            $table->increments('id');
            $table->Integer('user_id');
            $table->string('name');
            $table->string('company');
            $table->string('address');
            $table->Integer('phone_number');
            $table->string('email');
            $table->string('notes');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
