@extends('app')

@section('content')
    <!-- Name Form Input -->
    {!! Form::open() !!}
        <div class="form-group">
            {!! Form::label('name', 'Name *:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    <!-- Company Form Input -->
    <div class="form-group">
        {!! Form::label('company', 'Company:') !!}
        {!! Form::text('company', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Address Form Input -->
    <div class="form-group">
        {!! Form::label('address', 'Address:') !!}
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Phone number Form Input -->
    <div class="form-group">
        {!! Form::label('phone_number', 'Phone number *:') !!}
        {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Email Form Input -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Notes Form Input -->
    <div class="form-group">
        {!! Form::label('notes', 'Notes:') !!}
        {!! Form::text('notes', null, ['class' => 'form-control']) !!}
    </div>
    <!--  Create Form Input -->
    <div class="form-group">
        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    <p>(*) Mandatory fields</p>
    {!! Form::close() !!}
    @include('errors.list')

@endsection