@extends('app')

@section('content')
    <h1 class="page-heading">Your Contacts</h1>
    <div class="panel panel-default">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                 {{--Table headings--}}
                <th>Name</th>
                <th>Company</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Notes</th>
            </tr>
            </thead>
            <tbody>
                @foreach($records as $record)
                    {!! Form::open(['route' => ['delete_record', $record->id], 'method' => 'DELETE']) !!}
                        <tr id="{!! $record->id !!}">
                            {{--Table records--}}
                            <td>{!! $record->name !!}</td>
                            <td>{!! $record->company !!}</td>
                            <td>{!! $record->address !!}</td>
                            <td>{!! $record->phone_number !!}</td>
                            <td>{!! $record->email !!}</td>
                            <td>{!! $record->notes !!}</td>
                            <td><button type="submit" class="btn btn-danger">Delete</button></td>
                        </tr>
                    {!! Form::close() !!}
                @endforeach
            </tbody>
        </table>
        {{--Message when the record is deleted--}}
        @include('errors.message')
    </div>
@endsection